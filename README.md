# Quectel_BG96_QuecOpen_SDK_Package

#### 项目介绍
移远BG96SDK

#### 软件架构
```shell
.
├── bin
│   ├── oem_app_path.ini
│   ├── quectel_demo_uart.bin
│   └── quectel_demo_uart.elf
├── build_quectel_demo_app.bat
├── build_sensor_demo_app.bat
├── build_sensor_demo_app.sh
├── include
│   ├── qapi
│   └── threadx_api
├── libs
│   ├── diag_dam_lib.lib
│   ├── qapi_psm_lib.lib
│   ├── timer_dam_lib.lib
│   └── txm_lib.lib
├── quectel
│   ├── build
│   ├── example  # 模块示例代码，例如任务创建、定时器、GPIO、HTTP、MQTT、GPS 等等
│   └── utils
├── Sample_Applications
│   ├── data_common
│   ├── dns
│   ├── hello_world
│   ├── location
│   └── mqtt
├── sensor
│   ├── build
│   └── src
├── tags
└── -Wl
```




#### 安装教程

1. xxxx
2. xxxx
3. xxxx

#### 使用说明

1. xxxx
2. xxxx
3. xxxx

